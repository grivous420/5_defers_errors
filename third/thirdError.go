package third

import "fmt"

type database struct{}

func (db *database) connect() (disconnect func()) {
	fmt.Println("connect")
	return func() {
		fmt.Println("disconnect")
	}
}

// DeferAsAWrapper #3 — Defer as a wrapper
// Sometimes you need to defer with closures to be more practical or some other reasons I cannot guess right now. For example, to open a database connection, then run some queries and at the end to ensure that it gets disconnected.
// Why it doesn’t work?
// It doesn’t disconnect and it connects at the end, which is a bug. Only thing that happened here is that connect() gets saved aside until the func ends and it doesn’t run.
func DeferAsAWrapper() {
	wrong()
	right()
	badPracticeRight()
}

func wrong() {
	fmt.Println("wrong defer ...")

	db := &database{}
	defer db.connect()

	fmt.Println("query db...")
}

// Solution Now, db.connect() returns a func and we can use it with defer to disconnect from the database when the surrounding func ends.
func right() {
	db := &database{}
	fmt.Println("right defer ...")

	close := db.connect()
	defer close()

	fmt.Println("query db...")
}

// This code is technically almost the same as the above solution. Here, the first parenthesis is for connecting to the database (which happens immediately on defer db.connect()) and then the second parenthesis is for defer to run the disconnector func (the returned closure) when the surrounding func ends.
// This happens because db.connect() creates a value which is a closure and then defer registers it. The value of db.connect() needs to be resolved first to be registered with defer. This is not directly related with defer however it may solve some of your gotchas you may have.
func badPracticeRight() {
	fmt.Println("bad-practice right defer ...")

	db := &database{}
	defer db.connect()()

	fmt.Println("query db...")
}
