package first

import "fmt"

// DeferredNilFunc #1 If a deferred func evaluates to nil, execution panics when the surrounding func ends not when defer is called.
// Why?
// Here, the func continues until the end, after that the deferred func will run and panic because it’s nil. However, run() can be registered without a problem because it wouldn’t be called until the containing func ends.
// This is a simple example but the same thing can happen in the real-world, so if you get across something like this, be suspicious about that that could have been happened because of this gotcha.
func DeferredNilFunc() {
	var run func() = nil
	defer run()
	fmt.Println("runs")
}
