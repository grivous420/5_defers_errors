package main

import (
	"defers/fifth"
	"defers/first"
	"defers/fourth"
	"defers/second"
	"defers/third"
)

func main() {
	first.DeferredNilFunc()
	second.DeferInsideALoop()
	third.DeferAsAWrapper()
	fourth.DeferInABlock()
	fifth.DeferredMethodGotchas()
}
