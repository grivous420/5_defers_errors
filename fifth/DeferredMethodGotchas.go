package fifth

import "fmt"

type car struct {
	model string
}

func (c car) PrintModelNoPTR() {
	fmt.Println(c.model)
}

func (c *car) PrintModelWithPTR() {
	fmt.Println(c.model)
}

// DeferredMethodGotchas #5. You can also use methods with defer. However, there’s a quirk. Remember that the passed params to a deferred func are saved aside immediately without waiting for the deferred func to be run.
func DeferredMethodGotchas() {
	wrong()
	right()

}

// So, when a method with a value-receiver is used with defer, the receiver will be copied (in this case Car) at the time of registering and the changes to it wouldn’t be visible (Car.model). Because the receiver is also an input param and evaluated immediately to “DeLorean DMC-12” when it’s registered with the defer.
func wrong() {
	c := car{model: "DeLorean DMC-12"}
	defer c.PrintModelNoPTR()
	c.model = "Chevrolet Impala"
}

// On the other hand, when the receiver is a pointer when it’s called with defer, a new pointer is created but the address it points to would be the same with the “c” pointer above. So, any changes to it would be reflected flawlessly.
func right() {
	c := car{model: "DeLorean DMC-12"}
	defer c.PrintModelWithPTR()
	c.model = "Chevrolet Impala"
}
