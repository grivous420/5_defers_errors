package fourth

import "fmt"

// DeferInABlock #4. You may expect that a deferred func will run after a block ends but it does not, it only executes after the containing func ends. This is also true for all blocks: For, switch, etc except func blocks as we saw in the previous gotchas.
func DeferInABlock() {
	wrong()
	right()
}

func wrong() {
	{
		{
			defer func() {
				fmt.Println("block: defer runs")
			}()
			fmt.Println("block: ends")
		}
		fmt.Println("main: ends")
	}
}

// If you want to run defer in a block you can convert it to a func such as an anonymous func just like the solutions of gotcha #2.
func right() {
	func() {
		defer func() {
			fmt.Println("func: defer runs")
		}()
		fmt.Println("func: ends")
	}()
	fmt.Println("main: ends")
}
